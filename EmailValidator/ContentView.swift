//
//  ContentView.swift
//  EmailValidator
//
//  Created by Radu Petrisel on 02.03.2023.
//

import SwiftUI

struct ContentView: View {
    @State var isEmailValid: Bool = true
    
    var body: some View {
        VStack {
            EmailField(isValid: $isEmailValid)
            
            if isEmailValid {
                Text("Email is valid")
                    .foregroundColor(.green)
            } else {
                Text("Email is not valid")
                    .foregroundColor(.red)
            }
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//
//  EmailValidatorApp.swift
//  EmailValidator
//
//  Created by Radu Petrisel on 02.03.2023.
//

import SwiftUI

@main
struct EmailValidatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

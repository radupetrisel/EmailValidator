//
//  EmailValidator.swift
//  EmailValidator
//
//  Created by Radu Petrisel on 05.03.2023.
//

import Foundation
import RegexBuilder

struct EmailValidator {
    private let regex: Regex<(Substring, Substring, Substring)>
    
    init() {
        regex = Regex {
            Capture {
                ZeroOrMore {
                    OneOrMore(.word)
                    "."
                }
                OneOrMore(.word)
            }
            "@"
            Capture {
                OneOrMore(.word)
                ZeroOrMore {
                    "."
                    OneOrMore(.word)
                }
            }
        }
    }
    
    func match(_ string: String) -> Bool {
        string.wholeMatch(of: regex)?.output != nil
    }
}

//
//  EmailField.swift
//  EmailValidator
//
//  Created by Radu Petrisel on 02.03.2023.
//

import SwiftUI

struct EmailField: View {
    @Binding var isValid: Bool
    
    @FocusState private var isFocused: Bool
    @State private var color: Color = .black
    @State private var email: String = ""
    
    private let validator: EmailValidator = .init()
    
    var body: some View {
        TextField("Email", text: $email)
            .textContentType(.emailAddress)
            .textInputAutocapitalization(.never)
            .keyboardType(.emailAddress)
            .foregroundColor(color)
            .padding(5)
            .border(color, width: 1)
            .padding()
            .focused($isFocused)
            .onChange(of: isFocused) { value in
                if !value {
                    isValid = validator.match(email)
                }
            }
    }
}

struct EmailField_Previews: PreviewProvider {
    static var email: String = ""
    static var isValid: Bool = true
    
    static var previews: some View {
        EmailField(
            isValid: Binding(get: { isValid }, set: { value in isValid = value  }))
    }
}
